function persona(nombre,edad,DNI,sexo,peso,altura){
    this.nombre = nombre;
    this.edad = edad;
    this.DNI = DNI;
    this.sexo = sexo;
    this.peso = peso;
    this.altura = altura;

    const BAJO_PESO = -1;
    const PESO_MEDIO = 0;
    const SOBREPESO = 1;

    this.calcularMC= function(){
   	 let calcular = (peso/(altura*2));
   	 if(calcular < 20){
   		 return BAJO_PESO;
   	 } else if(calcular >= 20 && calcular <= 25){
   		 return PESO_MEDIO;
   	 } else if(calcular >25){
   		 return SOBREPESO;
   	 }else
   		 console.log("Ingresó un valor no válido")
    }
    
    this.esMayordeEdad= function(){
   	 if(edad >= 18){
   		 return true;
   	 }else
   		 return false;
    }

    this.comprobarSexo= function() {
        if (sexo == "H" || sexo == "M"){
            console.log ("Sexo ingresado correctamente")
        } else {
            console.log ("El sexo se agrego en hombre, pero la letra ingresada fue incorrecta")
        }
    }
}